#include <Bounce2.h>

#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"
#include <avr/pgmspace.h>
#include "messages.h"
#include "eeprom_config.h"

#define MAX_WAIT_MS 2000
#define BUTTON_CONNECT 3
#define BUTTON_SWITCH 2

#define DEBOUNCE_INTERVAL 100
RF24 radio(9, 10);

const byte BROADCAST[] = "FFFFF";

config_t conf;

msg_payload msg_buf;

Bounce switchButton = Bounce();
Bounce connButton = Bounce();

void radio_setup() {
  radio.openReadingPipe(0, BROADCAST);
  radio.openReadingPipe(1, conf.own_addr);
  radio.startListening();
}

void setup() {
  Serial.begin(115200);
  printf_begin();

  setup_config(conf);

  pinMode(BUTTON_CONNECT, INPUT_PULLUP);
  connButton.attach(BUTTON_CONNECT);
  connButton.interval(DEBOUNCE_INTERVAL);

  pinMode(BUTTON_SWITCH, INPUT_PULLUP);
  switchButton.attach(BUTTON_SWITCH);
  switchButton.interval(DEBOUNCE_INTERVAL);

  radio.begin();
  radio.setPALevel(RF24_PA_LOW);
  radio_setup();
  radio.printDetails();

  get_info();
}

bool writeMsg(msg_type type, byte state) {
  msg_buf.type = type;
  msg_buf.state = state;
  memcpy(msg_buf.addr, conf.own_addr, 5);
  return radio.write(&msg_buf, sizeof(msg_buf));
}

void readMsg() {
  radio.read(&msg_buf, sizeof(msg_buf));
}

void set_remote_addr() {
  radio.stopListening();
  radio.openWritingPipe(BROADCAST);

  unsigned long st = millis(), dt = -1;
  while (!writeMsg(ID_REQUEST, 0) && (dt = millis() - st) <= MAX_WAIT_MS) {
    Serial.println(F("Write of ID_REQUEST failed."));
    delay(250);
  }
  radio_setup();
  uint8_t pipeNum = 10;
  while (!radio.available(&pipeNum) && pipeNum != 1 && (dt = millis() - st) <= MAX_WAIT_MS) {
    Serial.println(F("Awaiting ID_RESPONSE."));
    delay(20);
  }
  if (dt > MAX_WAIT_MS) {
    Serial.println(F("Await time expired"));
    return;
  }
  readMsg();
  memcpy(conf.remote_addr, msg_buf.addr, 5);
  conf.is_remote_set = true;
  EEPROM.put(EE_CONF_ADDR, conf);
  Serial.println(F("Got ID_RESPONSE."));
}

void switch_remote() {
  if (!conf.is_remote_set) {
    Serial.println(F("Address not set"));
    return;
  }
  radio.stopListening();
  radio.openWritingPipe(conf.remote_addr);
  if (!writeMsg(SWITCH_REQUEST, -1*msg_buf.state + 1)) {
    Serial.println(F("Write of SWITCH_REQUEST failed."));
  }
  radio.startListening();
}


void get_info() {
  if (!conf.is_remote_set)
    return;
  radio.stopListening();
  radio.openWritingPipe(conf.remote_addr);
  if (!writeMsg(STATE_REQUEST, msg_buf.state)) {
    Serial.println(F("Write of STATE_REQUEST failed."));
  }
  radio.startListening();
}

void loop() {
  connButton.update();
  switchButton.update();
  
  if (connButton.fell()) {
    set_remote_addr();
  }

  if (switchButton.fell()) {
    switch_remote();
  }
  
  if (radio.available()) {
    readMsg();
    if (msg_buf.type == STATE_INFO && memcmp(msg_buf.addr, conf.remote_addr, 5) == 0) {
      printf("Light is %s\n", msg_buf.state ? "on":"off");
    }
  }

  if (Serial.available()) {
    char op = Serial.read();
    switch (op) {
    case 's':
    case 'S':
      switch_remote();
      break;
    case 'c':
    case 'C':
      set_remote_addr();
      break;
    case 'i':
    case 'I':
      get_info();
      break;
    default:
      Serial.println(F("Unknown command"));
    }
  }
}
