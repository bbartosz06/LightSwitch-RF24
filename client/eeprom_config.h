#ifndef EEPROM_CONFIG_H
#define EEPROM_CONFIG_H 1
#include <EEPROM.h>
struct config_t
{
  byte own_addr[5];
  bool is_remote_set;
  byte remote_addr[5];
};

#define CONF_MARKER 12017
static const int EE_CONF_ADDR = sizeof(int);
void setup_config(config_t &conf) {
  int marker = 0;
  EEPROM.get(0, marker);
  if (marker == CONF_MARKER) {
    EEPROM.get(EE_CONF_ADDR, conf);
    Serial.println(F("Config read from file"));
  } else {
    EEPROM.put(0, CONF_MARKER);
    memcpy(conf.own_addr, "0Clie", 5);
    conf.is_remote_set = false;
    memcpy(conf.remote_addr, "\0\0\0\0\0", 5);
    EEPROM.put(EE_CONF_ADDR, conf);
    Serial.println(F("Default config set"));
  }
}
#endif