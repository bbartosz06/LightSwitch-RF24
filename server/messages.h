enum msg_type {
  ID_REQUEST  = 1,
  ID_RESPONSE = 2,
  STATE_REQUEST = 11,
  STATE_INFO = 12,
  SWITCH_REQUEST = 21
};

struct msg_payload {
  msg_type type;
  byte state;
  byte addr[5];
};

