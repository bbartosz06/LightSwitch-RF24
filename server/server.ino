#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include <Bounce2.h>
#include "printf.h"
#include <avr/pgmspace.h>
#include "messages.h"

#define LED_PIN 5
#define BUTTON_CONNECT 4

#define DEBOUNCE_INTERVAL 100
RF24 radio(9, 10);

const byte BROADCAST[] = "FFFFF";
const byte OWN_ADDR[] = "0Serv";
// const byte TEST_ADDR[] = "1Serv";
//first byte of addr is server's id

byte tmp_addr[5];

msg_payload msg_buf;

byte led_status;
Bounce connButton = Bounce();

unsigned long ctime = -1L;
bool is_connection_allowed = false;
#define CONNECT_MODE_TIME 10000

void radio_setup() {
  if (is_connection_allowed) {
   radio.openReadingPipe(0, BROADCAST);
  } else {
    radio.closeReadingPipe(0);
  }
  radio.openReadingPipe(1, OWN_ADDR);
  // radio.openReadingPipe(2, TEST_ADDR);
  radio.startListening();
}

void setup() {
  Serial.begin(115200);
  printf_begin();

  pinMode(LED_PIN, OUTPUT);
  led_status = 0;

  pinMode(BUTTON_CONNECT, INPUT_PULLUP);
  connButton.attach(BUTTON_CONNECT);
  connButton.interval(DEBOUNCE_INTERVAL);

  radio.begin();
  radio.setPALevel(RF24_PA_LOW);
  radio_setup();
  radio.printDetails();

}

bool writeMsg(msg_type type, byte state) {
  msg_buf.type = type;
  msg_buf.state = state;
  memcpy(msg_buf.addr, OWN_ADDR, 5);
  return radio.write(&msg_buf, sizeof(msg_buf));
}

void readMsg() {
  radio.read(&msg_buf, sizeof(msg_buf));
}

void switch_led() {
  set_led(-1*led_status + 1);
}

void set_led(byte st) {
  led_status = st;
  digitalWrite(LED_PIN, led_status);
}

void allow_connections() {
  ctime = millis();
  is_connection_allowed = true;
  radio_setup();
  printf("Connections allowed for %d ms\n", CONNECT_MODE_TIME);
}

void update_connetions_status() {
  if (is_connection_allowed && millis() - ctime > CONNECT_MODE_TIME) {
    is_connection_allowed = false;
    Serial.println(F("Connections not allowed anymore"));
  }
}


void loop() {
  connButton.update();
  update_connetions_status();
  if (connButton.fell()) {
    allow_connections();
  }

  if (Serial.available()) {
    char op = Serial.read();
    switch (op) {
    case 's':
    case 'S':
      switch_led();
      radio.openWritingPipe(BROADCAST);
      writeMsg(STATE_INFO, led_status);
      break;
    case 'c':
    case 'C':
      allow_connections();
      break;
    default:
      Serial.println(F("Unknown command"));
    }
  }

  if (radio.available()) {
    readMsg();
    radio.stopListening();
    memcpy(tmp_addr, msg_buf.addr, 5);
    switch (msg_buf.type) {
      case ID_REQUEST:
        Serial.println(F("Got ID_REQUEST"));
        radio.openWritingPipe(tmp_addr);
        if (is_connection_allowed) {
          writeMsg(ID_RESPONSE, led_status);
          Serial.println(F("Responded with ID"));
        }
        break;
      case STATE_REQUEST:
        radio.openWritingPipe(tmp_addr);
        writeMsg(STATE_INFO, led_status);
        Serial.println(F("Got STATE_REQUEST"));
        break;
      case SWITCH_REQUEST:
        radio.openWritingPipe(BROADCAST);
        if (msg_buf.state != led_status) {
          set_led(msg_buf.state);
        }
        writeMsg(STATE_INFO, led_status);
        Serial.print(F("Got SWITCH_REQUEST, "));
        printf("light is now %s\n", led_status ? "on":"off");
        break;
      default:
        Serial.println(F("Something went wrong, reached default clause"));
    }
    radio_setup();
  }

}
